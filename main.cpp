#include <iostream>
#include <fstream>

using namespace std;

void display_file_choice(fstream &file);

void int_cin_fail_clear();

void display_file(fstream &file, string lesson_name);

void save_to_file(fstream &file, const string &name_input, const string &surname_input, const string &month_input,
                  int date_input, int mark_input);

int main() {
    int display_choice;
    int date_input;
    int mark_input;
    int exit_repeat_display_choice = 0;

    string lesson_input;
    string name_input;
    string surname_input;
    string month_input;
    fstream file;

    cout << "---DZIENNIK ELEKTRONICZNY---" << endl;

    do {
        cout << "1.Wyswietl dziennik" << endl;
        cout << "2.Dodaj ocene" << endl;
        cin >> display_choice;

        while (display_choice < 1 || display_choice > 2 || cin.fail()) {
            int_cin_fail_clear();
            cout << R"(Wpisz "1" lub "2")" << endl;
            cin >> display_choice;
        }
        if (display_choice == 1) {
            display_file_choice(file);

        }
    } while (display_choice != 2);

    do {

        cout << "Podaj przedmiot" << endl;
        cin >> lesson_input;
        for (auto &i: lesson_input) i = tolower(i);

        for (;;) {
            if (lesson_input == "matematyka" || lesson_input == "biologia" || lesson_input == "chemia") {
                break;
            }
            cout << "Wprowadzono niepoprawny przedmiot" << endl;
            cin >> lesson_input;
            lesson_input[0] = toupper(lesson_input[0]);
        }

        cout << "Podaj imie i nazwisko" << endl;
        cin >> name_input;
        cin >> surname_input;
        name_input[0] = toupper(name_input[0]);
        surname_input[0] = toupper(surname_input[0]);
        cout << "Wpisz miesiac" << endl;
        cin >> month_input;
        for (auto &i: month_input) i = tolower(i);
        for (;;) {
            if (month_input == "styczen" || month_input == "luty" || month_input == "marzec" ||
                month_input == "kwiecien" || month_input == "maj" ||
                month_input == "czerwiec" || month_input == "lipiec" || month_input == "sierpien" ||
                month_input == "wrzesien" ||
                month_input == "pazdziernik" || month_input == "listopad" || month_input == "grudzien") {
                break;
            }
            cout << "Wprowadzono niepoprawny miesiac" << endl;
            cin >> month_input;
            month_input[0] = tolower(month_input[0]);
        }

        cout << "Wpisz dzien miesiaca" << endl;
        cin >> date_input;
        if (month_input == "styczen" || month_input == "marzec" || month_input == "maj" || month_input == "lipiec" ||
            month_input == "sierpien" ||
            month_input == "pazdziernik" || month_input == "grudzien") {
            while (date_input < 1 || date_input > 31 || cin.fail()) {
                int_cin_fail_clear();
                cout << "Wpisano niepoprawny dzien miesiaca" << endl;
                cin >> date_input;
            }
        }

        if (month_input == "kwiecien" || month_input == "czerwiec" || month_input == "wrzesien" ||
            month_input == "listopad") {
            while (date_input < 1 || date_input > 30 || cin.fail()) {
                int_cin_fail_clear();
                cout << "Wpisano niepoprawny dzien miesiaca" << endl;
                cin >> date_input;
            }
        }

        if (month_input == "luty") {
            while (date_input < 1 || date_input > 29 || cin.fail()) {
                int_cin_fail_clear();
                cout << "Wpisano niepoprawny dzien miesiaca" << endl;
                cin >> date_input;
            }
        }

        cout << "Wpisz ocene" << endl;
        cin >> mark_input;
        while (mark_input < 1 || mark_input > 6 || cin.fail()) {
            int_cin_fail_clear();
            cout << "Wpisano niepoprawna ocene" << endl;
            cin >> mark_input;
        }

        if (lesson_input == "biologia") {
            file.open("C:\\Programs\\Biologia.txt", ios::app);
            save_to_file(file, name_input, surname_input, month_input, date_input, mark_input);
            file.close();
        }

        if (lesson_input == "chemia") {
            file.open("C:\\Programs\\Chemia.txt", ios::app);
            save_to_file(file, name_input, surname_input, month_input, date_input, mark_input);
            file.close();
        }

        if (lesson_input == "matematyka") {
            file.open("C:\\Programs\\Matematyka.txt", ios::app);
            save_to_file(file, name_input, surname_input, month_input, date_input, mark_input);
            file.close();
        }

        do {
            cout << "1.Wyswietl dziennik" << endl;
            cout << "2.Dodaj ocene" << endl;
            cout << "3.Wyjdz z programu" << endl;
            cin >> exit_repeat_display_choice;

            while (exit_repeat_display_choice < 1 || exit_repeat_display_choice > 3 || cin.fail()) {
                int_cin_fail_clear();
                cout << R"(Wpisz "1" lub "2" lub "3")" << endl;
                cin >> exit_repeat_display_choice;
            }

            if (exit_repeat_display_choice == 1) {
                display_file_choice(file);
            }
        } while (exit_repeat_display_choice == 1);

    } while (exit_repeat_display_choice != 3);

    return 0;
}

void display_file_choice(fstream &file) {
    int choice_file;
    string lesson_name;
    cout << "Wybierz przedmiot ktory chcesz wyswietlic" << endl;
    cout << "1.Matematyka" << endl;
    cout << "2.Biologia" << endl;
    cout << "3.Chemia" << endl;
    cin >> choice_file;

    while (choice_file < 1 || choice_file > 3 || cin.fail()) {
        int_cin_fail_clear();
        cout << R"(Wpisz "1" lub "2" lub "3")" << endl;
        cin >> choice_file;
    }

    if (choice_file == 1) {
        lesson_name = "Matematyka";
        file.open("C:\\Programs\\Matematyka.txt", ios::in);
        display_file(file, lesson_name);
        file.close();
    }

    if (choice_file == 2) {
        lesson_name = "Biologia";
        file.open("C:\\Programs\\Biologia.txt", ios::in);
        display_file(file, lesson_name);
        file.close();
    }

    if (choice_file == 3) {
        lesson_name = "Chemia";
        file.open("C:\\Programs\\Chemia.txt", ios::in);
        display_file(file, lesson_name);
        file.close();
    }
}

void int_cin_fail_clear() {
    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
}

void display_file(fstream &file, string lesson_name) {
    string line;
    file.seekg(0, ios::end);
    cout << endl;
    if (file.tellg() == 0) {
        cout << "Plik \"" << lesson_name << ".txt\" jest pusty" << endl;
    } else {
        for (auto &i: lesson_name) i = toupper(i);
        cout << "---" << lesson_name << "---" << endl;
        file.seekg(0);
        while (!file.eof()) {
            getline(file, line);
            cout << line << endl;
        }
    }
    cout << endl;
}

void save_to_file(fstream &file, const string &name_input, const string &surname_input, const string &month_input,
                  int date_input, int mark_input) {
    file.seekg(0, ios::end);
    if (file.tellg() == 0) {
        file << name_input << " " << surname_input << endl;
        file << date_input << " " << month_input << endl;
        file << "Ocena: " << mark_input;
    } else {
        file << endl << "----------" << endl;
        file << name_input << " " << surname_input << endl;
        file << date_input << " " << month_input << endl;
        file << "Ocena: " << mark_input;
    }
}
